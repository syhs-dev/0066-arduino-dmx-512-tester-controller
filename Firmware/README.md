# **Firmware:**

## ** Notas sobre la versi�n **

***

- Compilado en Arduino IDE v1.0.6
- instalaci�n de librer�as DMX:
	- Instalar Arduino IDE v1.0.6 (carpeta .../Software/) (Windows 10 64 bits, ok!)
	- Instalar la librer�a del encoder:
		- Menu Sketch, Importar Liberia, Add Library
		- Seleccionar el archivo Encoder.zip (carpeta .../Firmware/Documentacion/Librerias/)
		- Abrir
		- Mensaje: Library added to you libraries
	- Instalar la librer�a del LCD:
		- Men� Sketch, Importar Liberia, Add Library
		- Seleccionar el archivo LiquidCrystal_V1.2.1.zip (carpeta .../Firmware/Documentacion/Librerias/)
		- Abrir
		- Mensaje: Library added to you libraries
	- Instalar la librer�a del DMX:
		- Copiar la carpeta DMX desde .../Firmware/Documentacion/Librerias/DMX/Dmx/
		- Pegarla en la carpeta de .../Arduino/libraries/ el los archivos de programa (para el caso de Windows 10 64 bits, C:\Program Files (x86)\Arduino\libraries)
		- Reiniciar Arduino IDE
		- Revisar el men� Sketch, Importar librer�a..., Dmx
- Cargando el Firmware:
	- Seleccionar el firmware que vamos a cargar
	- Lo abrimos desde Arduino IDE
	- Men� Herramientas, Tarjeta, Arduino Mega 2560 or Mega ADK
	- Conectar el Arduino Mega al PC
	- Verificar que Windows lo d� de alta correctamente:
		- Panel de control\Sistema y seguridad\Sistema
		- Administrador de dispositivos
		- Puertos (COM y LPT)
		- Arduino Mega 2560 (COMx)
	- En Arduino IDE seleccionamos nuestro Arduino Mega:
		- Men� Herramientas, Puerto Serial, COMx
	- Clic en el icono Cargar...
	- Salen estos errores:
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: stk500v2_recv(): checksum error
		- avrdude: verification error, first mismatch at byte 0x0b60 0x50 != 0xd7
		- avrdude: verification error; content mismatch

***

## **vx.x - Beta**

- **CORREGIR,** void Numeric_Write, lo remplazo void Numerico_Print, esta funci�n ya no se utiliza
- **AGREGAR,** editor de secuencias con canales y valores espec�ficos
- **AGREGAR,** al editor de secuencias fade y tiempo de comando DMX entre ciclos
- **CORREGIR,** cambiar delay por interrupciones de timer
- **PENDIENTE,** funciones en teclado back light, agregar a lectura numerico write
- **PENDIENTE,** sacar num�rico write y remplazar por num�rico print
- **PENDIENTE,** Convert, cambiar cursor de navegaci�n de binarios
- **PENDIENTE,** DMX_Controller.ino: In function 'int Numerico_Write(int, int, byte, byte, byte, int)':
- **PENDIENTE,** DMX_Controller.ino:5971: warning: control reaches end of non-void function
- **PENDIENTE,** en Numerico_Write, ciclos if por else if
- **PENDIENTE,** en Navegar_Matrix, ciclos if por else if
- **PENDIENTE,** en Numerico_Enc_Write, ciclos if por else if
- **PENDIENTE,** en Numerico_Write, ciclos if por else if
- **PENDIENTE,** en convert bin, cuando se selecciona un dip switch, y se presiona enter para editar, y despu�s D, se queda con _ el dip switch
- **PENDIENTE,** control matrix, caracter - entre n�meros de donde a donde de matriz
- **PENDIENTE,** Chaser, blink, cuando est� corriendo
- **AGREGAR,** Chaser, valor del canal a secuenciar
- **PENDIENTE,** Secuencer, blink al correr, esta desubicado

***

## **vx.x - OSC to DMX**

- **AGREGADO,** Prueba con control v�a OSC
- **PENDIENTE,** en convert bin, cuando se selecciona un dip switch, y se presiona enter para editar, y después D, se queda con _ el dip switch
- **PENDIENTE,** control matrix, caracter - entre n�meros de donde a donde de matriz
- **PENDIENTE,** Chaser, blink, cuando est� corriendo
- **AGREGAR,** Chaser, valor del canal a secuenciar
- **PENDIENTE,** Secuencer, blink al correr, esta desubicado 

***

## **[vx.x - LCD Color - Touch Screen](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/vx.x/?at=master)**
- **AGREGADO,** Prueba con LCD de Color Touch Screen

***

## **[v3.4 - Beta - Hardware v1.0 - v1.x]()**

### **24-nov-18**
- **AGREGADO,** "F()" a cadenas de texto para guardar en rom y no en ram

### **24-nov-18**
- **AGREGADO,** matrix window, objetos y selecci�n 
- **AGREGADO,** matrix Secuencer, objetos y selecci�n 
- **AGREGADO,** matrix Chaser, objetos y selecci�n 
- **AGREGADO,** matrix Converter, objetos y selecci�n 
- **AGREGADO,** matrix Options, objetos y selecci�n 
- **AGREGADO,** matrix blackout, objetos y selecci�n 
- **AGREGADO,** matrix memory, objetos y selecci�n 
- **AGREGADO,** OLED SSD1306 en librer�a, la 09 muestra mal los datos

### **22-nov-18**
- **AGREGADO,** Init About
- **AGREGADO,** OLED SSD1309 en libreria
- **AGREGADO,** se cambio la librer�a u8glib por u8g2
- **AGREGADO,** se cambio la librería u8glib por u8g2

***

## **[v3.3 - Beta - Hardware v1.0 - v1.x]()**

- **AGREGADO,** OLED SSD1309

***

## **[v3.2 - Beta - Hardware v1.0 - v1.x]()**

- **CORREGIDO,** Separadores, mas limpio

***

## **[v3.1 - Beta - Hardware v1.0 - 1.x]()**

- **CORREGIDO,** separadores para gui nuevo
- **CORREGIDO,** de 21 a 36 items de control
- **CORREGIDO,** los items de memoria se eliminan

***

## **[v3.0 - Beta - Hardware v1.0 - 1.x]()**

- **AGREGADO,** items de control de memoria
- **AGREGADO,** items de selecci�n 
- **AGREGADO,** 21 items en matriz
- **AGREGADO,** Items para control de matriz
- **AGREGADO,** Libreria u8glib
- **CAMBIADO,** LCD de Texto por pantalla gr�fica 128x64

***

## **[v2.2 - Stable - Final Support - Hardware v0.7 - v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/master/Firmware/v2.2/)**

- **Tama�o binario del Sketch:** 41.998 bytes (de un m�ximo de 258.048 bytes)
- **CORREGIDO,** se agreg� a serial.print, F() para que cadena se guarde en la flash y no en la RAM

***

## **[v2.1 - Stable - Hardware v0.7 - v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/c476548b1773d4190de46427af87cd6a6b826830/Firmware/v2.1/?at=master)**

- **Tama�o binario del Sketch:** 41,590 bytes (de un m�ximo de 258,048 bytes)
- **CORREGIR,** void Numeric_Write, lo remplazo void Numerico_Print, esta funci�n ya no se utiliza
- **AGREGADO,** a men� config, eeprom default
- **CAMBIADO,** men� config, reorganizaci�n de items
- **AGREGADO,** a men� 2, about
- **AGREGADO,** a men� 2, config
- **AGREGADO,** a men� 2, bot�n de back
- **AGREGADO,** Men� 2
- **AGREGADO,** Men� 1, bot�n de next
- **CAMBIADO,** Men� 1, reubicaci�n de items
- **CAMBIADO,** Config ya no tiene la opci�n de About, about, queda en el men� principal
- **CORREGIDO,** GUI_Control_Options a GUI_Menu_1, se agregar�n m�s men�s de funciones
- **CORREGIDO,** Contrast_Init a Init_Contrast, se busca m�s r�pido
- **CORREGIDO,** Back_Light_Init a Init_Back_Light, se busca m�s r�pido
- **CORREGIDO,** EEPROM_Load_Init a Init_EEPROM_Load, se busca m�s r�pido
- **AGREGADO,** car�cter next
- **AGREGADO,** car�cter back 
- **AGREGADO,** items de selecci�n 
- **AGREGADO,** 21 items en matriz
- **AGREGADO,** Items para control de matriz
- **AGREGADO,** Libreria u8glib
- **CAMBIADO,** LCD de Texto por pantalla gr�fica 128x64

***

## **[v2.0 - Stable - Hardware v0.7 - v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v2.0/?at=master)**

- **AGREGADO,** a config, external light
- **CAMBIADO,** en config, redistribuci�n de men�
- **ELIMINADO,** interruptores de lights, los sustituye teclado
- **AGREGADO,** en teclado *, on off de external light
- **AGREGADO,** en teclado #, on off de backlight y key light
- **AGREGADO,** en Navegar, la lectura de * y # para lights
- **CORREGIDO,** en GUI_Memory_Init, la llamada a GUI_Control_Options(), era redundante
- **CORREGIDO,** en Multiply, espaciado
- **CAMBIADO,** en Secuencer por caracteres nuevos
- **CORREGIDO,** en Memory, espaciado
- **CAMBIADO,** en Memory por caracteres nuevos
- **CAMBIADO,** en config por caracteres nuevos
- **CAMBIADO,** en control chaser por caracteres nuevos
- **CORREGIDO,** en control matrix, el �ndice empieza en opci�n 1 y no en 3
- **CAMBIADO,** en control matrix por caracteres nuevos
- **CAMBIADO,** en control unitary por caracteres nuevos
- **CAMBIADO,** en convert por caracteres nuevos
- **CAMBIADO,** en control options por caracteres nuevos
- **CAMBIADO,** en about por caracteres nuevos
- **CAMBIADO,** en men� memory init por caracteres nuevos
- **CAMBIADO,** en men� memory bank por caracteres nuevos
- **AGREGADO,** car�cter para LCD de exit
- **AGREGADO,** car�cter para LCD de archivo
- **ELIMINADO,** a numeric write, * y # como control de n�meros 
- **AGREGADO,** tabuladores al c�digo
- **CORREGIDO,** Link de about de wiki al de Bitbucket https://goo.gl/7RsKo1
- **CORREGIDO,** about, versi�n de firmware de 1.9 a 2.0
- **AGREGADO,** a inicio, Load Bank:-, se quita b-
- **AGREGADO,** a Memory, Bank:-, se quita b-
- **CORREGIDO,** en Memory Bank, la leyenda Bank esta recorrida
- **CORREGIDO,** en Initial Memory, faltaba:
- **CORREGIDO,** en Memory, espacios y :
- **CORREGIDO,** en Secuencer, faltaba:
- **CORREGIDO,** DMX_Controller.ino:5747: warning: 'numero_total' may be used uninitialized in this function, se corrigi� poniendo = 0

***

## **[v1.9 - Stable - Hardware v0.7 - v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.9/?at=master)**

- **PENDIENTE,** - funciones en teclado back light, agregar a lectura numerico write
- **PENDIENTE,** - sacar num�rico write y remplazar por num�rico print
- **PENDIENTE,** - Convert, cambiar cursor de navegaci�n de binarios
- **PENDIENTE,** - tecla *, para backlight y key light, en esta versi�n no est� activa
- **PENDIENTE,** - sacar num�rico write y remplazar por num�rico print
- **PENDIENTE,** - Convert, cambiar cursor de navegaci�n de binarios
- **PENDIENTE,** - tecla *, para backlight y key light, en esta versi�n no est� activa
- **PENDIENTE,** - tecla #, quitarla de enter, no se usa, sustituye encoder center
- **PENDIENTE,** - DMX_Controller.ino: In function 'int Numerico_Write(int, int, byte, byte, byte, int)':
	- DMX_Controller.ino:5971: warning: control reaches end of non-void function
- **PENDIENTE,** - DMX_Controller.ino:5747: warning: 'numero_total' may be used uninitialized in this function
- **PENDIENTE,** - link de repositorio a bitbucket https://goo.gl/7RsKo1
- **AGREGADO,**  - modelo de programaci�n de LCD - Encoder - keypad
	- https://github.com/daniel3514/Arduino-LCD-Encoder-KeyPad-Examples/tree/master/Examples/LCD%20-%20Encoder%20-%20Key%20Pad/v0.0
- **AGREGADO,** canal actual a eeprom
- **AGREGADO,** canal actual salvar en eeprom cuando se sale de alg�n control
- **AGREGADO,** Control Unit, funci�n para escribir valores repetidos en la matriz, ejemplo si hay dos unos ambos se actualizan
- **AGREGADO,** Control Matrix, Ubicar, valor anterior al final
- **AGREGADO,** Control Matrix, ubicar a inicial y final
- **AGREGADO,** Control Matrix, mostrar canal a inicial y final
- **CORREGIDO,** men�, memoria, cambio de orden de items
- **CORREGIDO,** men�, Config, l�mites y vista
- **CORREGIDO,** Control Unitary, redise�o de GUI
- **AGREGADO,** Control Unitary, 8 unitarios distintos no consecutivos
- **AGREGADO,** Convert, opci�n de ubicar canal
- **CORREGIDO,** Convert, redise�o de interface gr�fica
- **CORREGIDO,** Convert, modelo de navegaci�n de binarios
- **CORREGIDO,** Control Chaser, redise�o de interface gr�fica
- **AGREGADO,** Control Chaser. canal actual como first
- **AGREGADO,** Control Chaser, canal actual inicia encendido
- **AGREGADO,** Control Chaser, repasar canales desde encoder
- **CORREGIDO,** Control Secuencer, redise�o de GUI
- **CORREGIDO,** Config, la ventana de acci�n del contraste se queda en los l�mites de 0 a 255
- **AGREGADO,** Secuencer, el universo inicial siempre es 1
- **AGREGADO,** Multiply, valores preestablecidos

***

## **[v1.8 - beta - Hardware v0.7 - v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.8/?at=master)**

- **CORREGIR,** cambiar delay por interrupciones de timer
- **CORREGIDO,** Pin Out para hardware v0.7
- **CORREGIDO,** Pin encoder center al 6
- **CORREGIDO,** Quitar la programaci�n del potenci�metro
- **CORREGIDO,** Quitar la programaci�n de los cursores
- **CORREGIDO,** pin 6 de encoder center a pull-up
- **CORREGIDO,** pin key light a 11
- **CORREGIDO,** pin contraste a 12
- **AGREGAR,** en multiplicador, el valor en tiempo real desde el potenci�metro o teclado
- **AGREGAR,** control chaser desde el potenci�metro, al girar recorre los canales
- **AGREGAR,** EEPROM Default en pin 9 como pull-up
- **AGREGAR,** dimmer para luz led como l�mpara en pin 10
- **AGREGADO,** a EEPROM, espacio para Ext Light
- **AGREGADO,** funci�n para jumper de eeprom default
- **AGREGADO,** dimmer de pantalla al inicio
- **AGREGADO,** �ndices a posiciones de LCD en el Excel

***

## **[v1.7 - Stable - Hardware v0.3 - v0.5](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.7/?at=master)**

- **CORREGIDO,** se separa la lectura desde el encoder como funci�n
- **CORREGIDO,** Quitar la programaci�n del potenci�metro
- **CORREGIDO,** Quitar la programaci�n de los cursores
- **CORREGIDO,** pin 6 de encoder center a pull-up
- **CORREGIDO,** pin key light a 11
- **CORREGIDO,** pin contraste a 12
- **AGREGAR,** en multiplicador, el valor en tiempo real desde el potenci�metro o teclado
- **AGREGAR,** control chaser desde el potenci�metro, al girar recorre los canales
- **AGREGAR,** EEPROM Default en pin 9 como pull-up
- **AGREGAR,** dimmer para luz led como l�mpara en pin 10
- **AGREGADO,** a EEPROM, espacio para Ext Light
- **AGREGADO,** funci�n para jumper de eeprom default
- **AGREGADO,** dimmer de pantalla al inicio
- **AGREGADO,** �ndices a posiciones de LCD en el Excel

***

## **[v1.6 - Stable - Hardware v0.3 - v0.5](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.6/?at=master)**

- **CORREGIDO,** se agrega un encoder KEYES Rotary encoder module KY-040 para remplazar el potenci�metro
- **AGREGADO,** el bot�n del centro es el mismo del encoder
- **AGREGADO,** encoder CLK en 6 DT en 7
- **CORREGIDO,** el pin del potenci�metro se elimina
- **AGREGADO,** encoder library 
	- https://www.pjrc.com/teensy/td_libs_Encoder.html
- **CORREGIDO,** lectura an�loga de valor, se agrega el valor anterior en la escritura del LCD
- **CORREGIDO,** lectura an�loga en valor el cursor blink se queda en la "a" y no sobre el numero

***

## **[v1.5 - Stable - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.5/?at=master)**

- Ultima versi�n con potenci�metro
- **CORREGIDO,** el cursor en el an�logo se mostraba el blink fuera del numero y se encimaba

***

## **[v1.4 - Stable - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.4/?at=master)**

- **CORREGIDO,** en about la versi�n actual
- **CORREGIDO,** las leyendas Ctrl cambian por Exit, el men� control ya no tiene la leyenda
- **CORREGIDO,** se agrega un encoder KEYES Rotary encoder module KY-040 para remplazar el potenci�metro
- **AGREGADO,** el bot�n del centro es el mismo del encoder
- **AGREGADO,** encoder CLK en 6 DT en 7
- **CORREGIDO,** el pin del potenciómetro se elimina
- **AGREGADO,** encoder library 
	- https://www.pjrc.com/teensy/td_libs_Encoder.html
- **CORREGIDO,** lectura análoga de valor, se agrega el valor anterior en la escritura del LCD
- **CORREGIDO,** lectura análoga en valor el cursor blink se queda en la "a" y no sobre el numero

***

## **[v1.3 - Stable - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.3/?at=master)**

- **CORREGIR,** en control matrix cuando se selecciona un canal de la esquina el cursor parpadeando queda fuera de lugar
- **AGREGADO,** Control de iluminaci�n para teclado
- **AGREGADO,** a EEPROM la posici�n 4092 para guardar key light
- **AGREGADO,** la variable global int EEPROM_Limit para controlar el l�mite de escritura de eeprom de universos
- **AGREGADO,** Control de iluminaci�n para teclado
- **AGREGADO,** a EEPROM la posici�n 4092 para guardar key light
- **AGREGADO,** la variable global int EEPROM_Limit para controlar el l�mite de escritura de eeprom de universos
- **AGREGADO,** EEPROM_Limit a todas las funciones de memoria EEPROM
- **AGREGADO,** el control * en el teclado controla el back light y el key light 
- **AGREGADO,** variable global Light_On_Off para control desde tecla * de la iluminacion de teclado y back
- **AGREGADO,** al about un dimmer en key y back durante el efecto binario
- **AGREGADO,** a la tecla de light * un dimmer de on y off para key y back
- **CORREGIDO,** en Back_Light_En, byte Back_Light_Value = EEPROM.read(513); el valor esta fuera de rango
- **CORREGIDO,** unused variable 'EEPROM_Add'
- **CORREGIDO,** en about "open hardware!" por "Open Source License:"

***

## **[v1.2 - Stable - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.2/?at=master)**

- **AGREGADO,** al arranque toma el banco de la eeprom previamente elegido desde config
- **AGREGADO,** a config el banco que lee a arranque
- **AGREGADO,** al about el banco actual
- **AGREGADO,** a about reacomodo de datos
- **AGREGADO,** a about licencias open source
- **AGREGADO,** a initial memory el bank actual
- **AGREGADO,** en EEPROM add 4093 para guardar banco a correr al inicio
- **AGREGADO,** Bank 8 solo tiene 509 canales disponibles
- **AGREGADO,** a EEPROM Load el l�mite en bank 8 de 4092
- **AGREGADO,** a EEPROM Save el l�mite en bank 8 de 4092
- **CORREGIDO,** GUI Config de LCD config solo a config
- **CORREGIDO,** GUI Config de Control a exit
- **AGREGADO,** a GUI Config opci�n bank init
- **AGREGADO,** EEPROM_Load_Init para cargar el universo pre configurado al inicio
- **CORREGIDO,** GUI about cambio de disposici�n y url por wiki
- **AGREGADO,** a EEPROM Load el límite en bank 8 de 4092
- **AGREGADO,** a EEPROM Save el límite en bank 8 de 4092
- **CORREGIDO,** GUI Config de LCD config solo a config
- **CORREGIDO,** GUI Config de Control a exit
- **AGREGADO,** a GUI Config opci�n bank init
- **AGREGADO,** EEPROM_Load_Init para cargar el universo pre configurado al inicio
- **CORREGIDO,** GUI about cambio de disposici�n y url por wiki
- **AGREGADO,** GUI_Licence a config

***

## **[v1.1 - Beta - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.1/?at=master)**

- **AGREGADO,** memory a control options
- **AGREGADO,** a memory gui ClearAll para borrar todos los bancos de la eeprom
- **AGREGADO,** a memory gui reacomodo de men�s

***

## **[v1.0 - Beta - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v1.0/?at=master)**

- **CORREGIDO,** seceuncer no tiene reversa
- **CORREGIDO,** secuencer no tiene salida de dmx, el uso de memoria eeprom descartaba valores
- **CORREGIDO,** GUI_Secuencer se cambi� el m�todo de lectura de memoria, se hace en tiempo real, evita over flow
- **CORREGIDO,** GUI_Secuencer se cambi� a GUI_Control_Secuencer
- **CORREGIDO,** tabulacion
- **CORREGIDO,** manejo de memoria EEPROM al guardar y leer el conteo es en 1 y no en 0
- **CORREGIDO,** GUI_Chasser el cursor blink no se mostraba despu�s de stop 
- **CORREGIDO,** GUI_Control_Options, el cursor no se mostraba en unitary
- **CORREGIDO,** disminuci�n de tiempo mensaje final de opciones de acceso a eeprom
- **AGREGADO,** lectura de bot�n "center" en el about
- **AGREGADO,** a GUI_Secuencer l�mite de delay a 100
- **CORREGIDO,** GUI_Secuencer se cambi� el m�todo de lectura de memoria, se hace en tiempo real, evita over flow
- **CORREGIDO,** GUI_Secuencer se cambi� a GUI_Control_Secuencer
- **CORREGIDO,** tabulacion
- **CORREGIDO,** manejo de memoria EEPROM al guardar y leer el conteo es en 1 y no en 0
- **CORREGIDO,** GUI_Chasser el cursor blink no se mostraba despues de stop 
- **CORREGIDO,** GUI_Control_Options, el cursor no se mostraba en unitary
- **CORREGIDO,** disminuci�n de tiempo mensaje final de opciones de acceso a eeprom
- **AGREGADO,** lectura de bot�n "center" en el about
- **AGREGADO,** a GUI_Secuencer l�mite de delay a 100
- **AGREGADO,** a GUI_Secuencer en la salida a control regresa al dmx los datos desde la RAM
- **AGREGADO,** a GUI_Unitary el canal anterior y siguiente en el gui

***

## **[v0.9 - Beta - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.9/?at=master)**

- **CORREGIR,** GUI_Secuencer provoca over flow y se reinicia, demasiada RAM usada
- **CORREGIDO,** GUI_Navegar se cambi� la variable Boton_Delay_Cursor de long a int
- **CORREGIDO,** Contrast_Init se limita a no menor de 150 el valor del pwm
- **CORREGIDO,** GUI_Config solo en control options
- **CORREGIDO,** esta encimado en control options unitary
- **CORREGIDO,** GUI Control Options se quit� iniciar:, no se usa
- **CORREGIDO,** se agreg� variable global BackLight_Add para direcci�n de EEPROM
- **CORREGIDO,** se agreg� variable global Contrast_Add para direcci�n de EEPROM
- **CORREGIDO,** Se quit� el efecto binario cuando se usa la memoria EEPROM
- **AGREGADO,** GUI_Secuencer limitado delay entre 0 y 999 x 100 mS
- **AGREGADO,** GUI_Secuencer limitado final y first entre 1 y 8
- **AGREGADO,** a GUI_About bink del cursor
- **AGREGADO,** GUI_Memory_Bank
- **AGREGADO,** GUI_Memory_Bank, regresa 1 si se selecciona salir para regresar al men� anterior
- **AGREGADO,** a GUI_Memory_Bank, retardo cuando entra al banco de memoria
- **AGREGADO,** a matrix el n�mero de banco actual, si el banco no se seleccion� muestra "-"
- **AGREGADO,** a memory options el n�mero de banco actual, si el banco no se seleccion� muestra "-"
- **AGREGADO,** a unitary control el n�mero de banco actual, si el banco no se seleccion� muestra "-"
- **AGREGADO,** a multiply control el n�mero de banco actual, si el banco no se seleccion� muestra "-"
- **AGREGADO,** a GUI Memory la configuraci�n
- **AGREGADO,** a GUI Control Options el n�mero de banco actual, si el banco no se seleccion� muestra "-"
- **AGREGADO,** a GUI Control Options la configuraci�n
- **AGREGADO,** a GUI_EEPROM_Empty la leyenda del banco que se est� modificando "RAM"
- **AGREGADO,** a GUI_EEPROM_Empty el blink del lcd para saber que est� trabajando
- **AGREGADO,** a EEPROM_Clear control de universos guardados
- **AGREGADO,** a EEPROM_Clear al final y al principio el n�mero de banco
- **AGREGADO,** a EEPROM_Clear el blink del lcd para saber que est� trabajando
- **AGREGADO,** a EEPROM_Clear regresa 1 si se selecciona �xito
- **AGREGADO,** a EEPROM_Clear m�s tiempo para mensaje final
- **AGREGADO,** a EEPROM_Load control de universos guardados
- **AGREGADO,** a EEPROM_Load, regresa 1 si se selecciona exit
- **AGREGADO,** a EEPROM_Load al final el n�mero de banco
- **AGREGADO,** a EEPROM_Load el blink del lcd para saber que est� trabajando
- **AGREGADO,** a EEPROM_Load m�s tiempo para mensaje final
- **AGREGADO,** a EEPROM_Save control de universos guardados
- **AGREGADO,** a EEPROM_Save al final el n�mero de banco
- **AGREGADO,** a EEPROM_Save, regresa 1 si se selecciona exit
- **AGREGADO,** a EEPROM_Save el blink del lcd para saber que est� trabajando
- **AGREGADO,** a EEPROM_Save m�s tiempo para mensaje final
- **AGREGADO,** a GUI_Memory_Init control de universos
- **AGREGADO,** a GUI_Memory_Init manejo cuando se selecciona exit en los bancos de memoria
- **AGREGADO,** a GUI_Memory control de salida desde bancos de memoria
- **AGREGADO,** a GUI_Memory salida inmediata despu�s de la lectura de opci�n
- **AGREGADO,** a GUI_About la leyenda open hardware
- **AGREGADO,** a GUI_About m�s tiempo para mostrar
- **AGREGADO,** GUI Secuencer
- **AGREGADO,** el banco 8 solo llegar�a a 500 por el contraste y el backlight
- **AGREGADO,** Los bancos de memoria no aplican a memory empty
- **AGREGADO,** GUI_Memory_Bank solo act�a en cambios de EEPROM

***

## **[v0.8 - Stable - Hardware v0.3 - v0.4](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.8/?at=master)**

- **AGREGADO,** compatibilidad con Hardware v0.3 - v0.4

***

## **[v0.7 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.7/?at=master)**

- **AGREGADO,** en chaser el cursor en blink cuando hace la secuencia
- **AGREGADO,** funci�n de Black Out desde el men� de Memory, pone todos los canales en 0 y los regresa a lo que ten�an, para salir dar en center
- **CORREGIDO,** en config contraste el l�mite inferior no se mostraba
- **CORREGIDO,** en gui about se retir� variable id, no se usa
- **CORREGIDO,** en chaser se quit� el label iniciar, no se usa
- **CORREGIDO,** en num�rico calc se quit� la variable salida, no se usa
- **CORREGIDO,** en num�rico calc se quit� la variable Num_Val_Temp_3, no se usa
- **CORREGIDO,** en chaser ya no guarda los valores de los cambios aplicados, al salir los regresa a su lugar
- **CORREGIDO,** en multiply cuando hace en c�lculo se cambia apply por calc..
- **CORREGIDO,** en ubicar se agrega cancelar con center
- **CORREGIDO,** se cambi� la url del about apuntando al Wiki

***

## **[v0.6 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.6/?at=master)**

- **CORREGIDO,** los valores limites superiores de config se escrib�an un car�cter despu�s
- **CORREGIDO,** hacer a partir de 155 el dimmer del contraste
- **CORREGIDO,** bot�n * de back light funciona err�tico - faltaba delay para evitar repetici�n
- **CORREGIDO,** quitar en config el dimmer 0-255, no aplica al contraste
- **CORREGIDO,** las io no usadas son salidas low
- **CORREGIDO,** no inicializa el contraste
- **CORREGIDO,** en matrix no se aplican los cambios en los bancos
- **CORREGIDO,** en matrix el valor final del banco se pasa de 512
- **CORREGIDO,** en config backlight el valor no se guardaba en eeprom
- **CORREGIDO,** en config backlight se recorr�an todas las opciones de botones, se agreg� mecanismo para sacar del bucle
- **CORREGIDO,** se baj� el delay de la lectura an�loga para evitar pasos escalonados
- **AGREGADO,** potenci�metro para valores DMX con tecla "D" solo en valores, para detenerlo presionar # o center
- **AGREGADO,** an�logo a unitary
- **AGREGADO,** an�logo a matrix
- **AGREGADO,** an�logo a config back light
- **AGREGADO,** an�logo a config contrast
- **AGREGADO,** en la lectura an�logo mecanismo para escribir el valor solo cuando cambia y no todo el tiempo
- **AGREGADO,** funci�n de Black Out desde el men� de Memory, pone todos los canales en 0 y los regresa a lo que tenían, para salir dar en center
- **CORREGIDO,** en config contraste el l�mite inferior no se mostraba
- **CORREGIDO,** en gui about se retira variable id, no se usa
- **CORREGIDO,** en chaser se quit� el label iniciar, no se usa
- **CORREGIDO,** en num�rico calc se quit� la variable salida, no se usa
- **CORREGIDO,** en num�rico calc se quit� la variable Num_Val_Temp_3, no se usa
- **CORREGIDO,** en chaser ya no guarda los valores de los cambios aplicados, al salir los regresa a su lugar
- **CORREGIDO,** en multiply cuando hace en calculo se cambia apply por calc..
- **CORREGIDO,** en ubicar se agrega cancelar con center
- **CORREGIDO,** se cambi� la url del about apuntando al Wiki

***

## **[v0.5 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.5/?at=master)**

- **AGREGADO,** la funci�n para cuando se da enter en un numero para cambiarlo, si de da enter de nuevo se quede el n�mero que estaba
- **CORREGIDO,** en config backlight se recorr�an todas las opciones de botones, se agreg� mecanismo para sacar del bucle
- **CORREGIDO,** se baj� el delay de la lectura an�loga para evitar pasos escalonados
- **AGREGADO,** potenci�metro para valores DMX con tecla "D" solo en valores, para detenerlo presionar # o center
- **AGREGADO,** an�logo a unitary
- **AGREGADO,** an�logo a matrix
- **AGREGADO,** an�logo a config back light
- **AGREGADO,** an�logo a config contrast
- **AGREGADO,** en la lectura análogo mecanismo para escribir el valor solo cuando cambia y no todo el tiempo

***

## **[v0.4 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.4/?at=master)**

- **CORREGIDO,** se retira del about el ID
- **CORREGIDO,** se cambia en config el men� exit por Ctrl
- **AGREGADO,** void Back_Light_En() para encender o apagar el back light desde el teclado num�rico
- **AGREGADO,** void Back_Light_Init() para encender o apagar el back light desde la �ltima configuraci�n
- **AGREGADO,** a Control Options el men� Config
- **AGREGADO,** void GUI_Config() con las opciones de la configuraci�n, el LCD y la salida al pwm
- **AGREGADO,** control del back light a gui_navegar, siempre est� activo
- **AGREGADO,** se actualiza el about con el nombre del proyecto
- **AGREGADO,** al config el about
- **AGREGADO,** al about desde config espera a que se presione center para salir
- **AGREGADO,** al config mas info sobre el backlight
- **AGREGADO,** a control options Memory options
- **AGREGADO,** en matrix la opci�n de poner el valor final de la matriz
- **AGREGADO,** a la lectura num�rica centro como enter

***

## ** [v0.3 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.3/?at=master)**

- **CORREGIDO,** retirar de setup las variables de la versi�n y pasarlas directamente a su void (no hay raz�n para que sean globales)
- **CORREGIDO,** en gui about retirar el delay en el efecto binario
- **CORREGIDO,** eeprom save, load empty y clear retirar delay de efecto (se retir� en todos los casos)
- **CORREGIDO,** agregar al inicio del programa .ino un about
- **CORREGIDO,** en control chaser se quit� el espacio entre delay el n�mero y x10=mS, parece que fuera una opci�n

***

## **[v0.2 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.2/?at=master)**
- **CORREGIR,** retirar del setup las variables de la versi�n y pasarlas directamente a su void
- **AGREGADO,** en matrix la opci�n de poner el valor final de la matriz
- **AGREGADO,** a la lectura num�rica centro como enter
- **CORREGIR,** en GUI_About se retirar el delay en el efecto binario
- **CORREGIR,** eeprom save, load empty y clear retirar el delay de efecto binario
- **CORREGIR,** agregar al inicio del programa un about
- **CORREGIR,** checar el tema del puerto de data flux, no se asigna
- **CORREGIDO,** se cambi� gui_init por gui_about
- **CORREGIDO,** se cambia en unitary control, matrix por control y apunta a gui control options
- **CORREGIDO,** se cambia en matrix control, �ubi� por �con� y apunta a gui control options
- **CORREGIDO,** se quita la opci�n about es muy repetitiva
- **AGREGADO,** control options about
- **AGREGADO,** control options Chaser
- **AGREGADO,** control options multiply
- **AGREGADO,** actualizaci�n del texto "about"
- **AGREGADO,** actualizaci�n del texto �memory initial�
- **AGREGADO,** al GUI_Memory "empty" para borrar la RAM
- **AGREGADO,** el control multiply
- **AGREGADO,** a about al inicio el efecto del binario corriendo

***

## **[v0.1 - Stable - Hardware v0.0 - v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Firmware/v0.1/DMX_Controller/?at=master)**

- **CORREGIDO,** cambiar en gui unit a "Unitary"
- **CORREGIDO,** cambiar en GUI multi a "Matrix"
- **CORREGIDO,** hacer los barridos de la memoria m�s r�pidos, se agrega efecto de carga y no baja la velocidad
- **CORREGIDO,** agregar al men� memory "Cancel"
- **CORREGIDO,** se agrega 100ms mas a la lectura de los cursores
- **CORREGIDO,** cuando se escribe un numero en la matriz o en unit, mayor o menor al l�mite la variable funciona pero el visual no
- **CORREGIR,** en GUI_Multi no hay vista de posici�n
- **AGREGADO,** l�neas bajas cuando se est� escribiendo un n�mero, se quitaron los ceros
- **AGREGADO,** se implementa para el caso de los valores "A" para 255 y "B" para 000
- **AGREGADO,** se implementa para los valores la tecla "C" para ubicar la l�mpara, parpadea y deja de hacerlo al volverla a presionar
- **AGREGADO,** se implementa en la matriz el valor "C" que indica el canal donde est� posicionado el cursor

***

## **v0.0 - Beta - Hardware v0.0 - v0.2**

- **AGREGADO,** Hello World !!

***
