## **Licenciamiento:**

![](https://bytebucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/raw/d30550981509dc89a050d3633af97093fef2db77/Social/Logos/GPLv3.png)
[GNU General Public Licence Version 3](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Licencias/Licence%20-%20Firmware.md?at=master)

![](https://bytebucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/raw/d30550981509dc89a050d3633af97093fef2db77/Social/Logos/oshw.png)
[Open Source Hardware (OSHW) v1.0](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Licencias/Licence%20-%20Hardware.md?at=master)

***