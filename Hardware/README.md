# **Hardware:**

## **Notas sobre la versión**

***

## **[v1.0.1 - Beta - no firmware](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/master/Hardware/v1.0.1/)**
![](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/raw/976468ce3cf646f3a58f4f5baa4571adbe06600c/Hardware/v1.0.1/Hardware%20Definition/Hardware%20Definition%20v1.0.1.png)
### **10-may-19**
Se redefine todo el sistema, para lograr multiples protocolos de red, fisicos y maneras de comunicacion con el sistema:
	- Control de Bateria y carga con TP4656
		- https://dlnmh9ip6v2uc.cloudfront.net/datasheets/Prototyping/TP4056.pdf
	- Pantalla e interface micro SD con Adafruit HXD8357D 
		- https://learn.adafruit.com/adafruit-3-5-color-320x480-tft-touchscreen-breakout/overview
			- 3.5" TFT 320x480 
			- Micro SD Socket
	- Touchscreen Breakout Board STMPE610 - Resistive Touch Screen Controller
		- https://www.adafruit.com/products/1571
	- Control de graficos de pantalla con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
	- El control de las comunicaciones centralizado con Mega 2560 PRO CH340G/ATmega2560-16AU
		- https://robotdyn.com/mega-2560-pro-embed-ch340g-atmega2560-16au.html	
			- Comunicacion interna por Half Duplex via RS-485, esto asegura la comunicacion en tiempo real con todos los clientes internos
			- USB Serial Terminal
			- Iluminacion para teclado
			- Iluminacion externa
			- Manejo con encoder
			- Manejo con teclado numerico
			- Manejo con teclado de 4 botones para opciones extra
	- Comunicacion Midi OSC WiFi con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
			- Comunicacion interna por Half Duplex via RS-485
	- Comunicacion Art-Net WiFi con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
			- Comunicacion interna por Half Duplex via RS-485
	- Comunicacion HTTP WiFi con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
			- Comunicacion interna por Half Duplex via RS-485
	- Comunicacion DMX-512 RS-485 bidireccional, optoacoplado con WIFI D1 MINI ESP8266 dev. board USB CP2104 y MAXM22511 con convertidor DC DC integrado
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
		- https://datasheets.maximintegrated.com/en/ds/MAXM22510-MAXM22511.pdf
			- Comunicacion interna por Half Duplex via RS-485
	- Comunicacion RF 433MHz para DMX remoto con HC-12, dedicado con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
		- https://www.elecrow.com/download/HC-12.pdf
			- Comunicacion interna por Half Duplex via RS-485
	- Comunicacion con Leds SPI dedicado, optoacoplado con WIFI D1 MINI ESP8266 dev. board, USB CP2104
		- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-cp2104.html
			- Comunicacion interna por Half Duplex via RS-485

***

## **[v1.0.0 - Beta - no firmware](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/master/Hardware/v1.0.0/)**
![](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/raw/d66081ca11c8ed05d21690ab2e2d543642e1beb1/Hardware/v1.0.0/Hardware%20Definition/Hardware%20Definition%20v1.0.0.png)
### **7-may-19**
- El control de las comunicaciones centralizado con Mega 2560 PRO CH340G/ATmega2560-16AU
- Comunicacion USB serial
- Iluminacion para teclado
- Iluminacion externa
- Manejo con encoder
- Manejo con teclado numerico
- Manejo con teclado de 4 botones para opciones extra
	- https://robotdyn.com/mega-2560-pro-embed-ch340g-atmega2560-16au.html
- Pantalla e interface micro SD con Adafruit HXD8357D - 3.5" TFT 320x480 + Touchscreen Breakout Board w/MicroSD Socket
	- https://learn.adafruit.com/adafruit-3-5-color-320x480-tft-touchscreen-breakout/overview
- Controlador para pantalla touch Adafruit STMPE610 - Resistive Touch Screen Controller 
	- https://www.adafruit.com/products/1571
- Control de graficos de pantalla con D1 MINI ESP8266 dev. board, USB CH340G
	- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-ch340g.html
- Comunicacion OSC WiFi con D1 MINI ESP8266 dev. board, USB CH340G
	- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-ch340g.html
- Comunicacion Art-Net WiFi con D1 MINI ESP8266 dev. board, USB CH340G
	- https://robotdyn.com/wifi-d1-mini-esp8266-dev-board-usb-ch340g.html
- Microcontrolador dedicado a DMX-512 RS-485 bidireccional, optoacoplado con Arduino UNO (microcontroller)
- Comunicacion RF 433MHz para DMX remoto con HC-12
	- https://www.elecrow.com/download/HC-12.pdf
- Comunicacion con Leds SPI dedicado con Arduino UNO (microcontroller)

***

## ** [v0.9 - Beta - Firm v1.8 - v2.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/master/Hardware/v0.9/)**
![](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/raw/1bbdb52f234545d4233041a6c0d56693d9d4d2a9/Hardware/v0.9/Esquematico/Esquematico.png)
### **6-may-19**
- **AGREGADO,** optoacoplados para RS-485 completo
- **AGREGADO,** ATMEGA328P, Led live
- **AGREGADO,** ATMEGA328P, para salida de DMX dedicada
- **ELIMINADO,** UART0, se queda para control desde USB
- **AGREGADO,** HC-12, diodo en serie de Vcc, para evitar calentamiento
- **AGREGADO,** HC-12, para transmision inalambrica a modulo remoto

### **5-may-19**
- **AGREGADO,** led live a ESP-07
- **AGREGADO,** convertidores de nivel TTL, para la conexxion entre ESP-07 y Arduino
- **AGREGADO,** ESP-07, para integracion con Art-Net, OSC y conectividad WiFi
- **AGREGADO,** conectores de salida
- **AGREGADO,** interruptor para invertir polaridad
- **AGREGADO,** salida 485
- **AGREGADO,** B0505S-2W, electronica necesaria
- **ELIMINADO,** AM1D-0505SA, lo remplaza B0505S-2W
- **AGREGADO,** encoder, capacitores para rebote de interruptores de encoder
- **ELIMINADO,** encoder rotativo KY-040, lo remplaza el encoder son la placa de keyes
	- https://es.aliexpress.com/wholesale?catId=0&initiative_id=AS_20190505110028&SearchText=encoder+rotativo
- **AGREGADO,** driver para key light
- **AGREGADO,** driver para external light
- **AGREGADO,** driver para backlight de LCD
- **AGREGADO,** driver para contraste de LCD
- **CORREGIDO,** el transistor 2N2222A, se remplazo por BC548A, es mas estandar
- **AGREGADO,** Teclado de membrana para iluminacion y otras opciones
	- https://es.aliexpress.com/store/product/4-5-6-Key-Matrix-Membrane-Switch-1x4-1x5-1x6-key-Matrix-Array-Keypad-Keyboard-Control/1762106_32951777117.html?spm=a219c.search0104.3.114.bcf86a68Ox7erL&ws_ab_test=searchweb0_0,searchweb201602_8_10065_10068_319_10059_10884_317_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_52,ppcSwitch_0&algo_expid=c578f494-fe24-4f96-98e8-29123ec311d7-16&algo_pvid=c578f494-fe24-4f96-98e8-29123ec311d7
- **ELIMINADO,** SW External light
- **ELIMINADO,** SW key light
- **ELIMINADO,** Jummper defauls EEPROM, se queda en el software
- **AGREGADO,** teclado numerico, conexion a Arduino 
- **AGREGADO,** manejo de ambas cargas de alimentacion, baterias y alimentacion externa
- **AGREGADO,** interruptor 2P2T, para on off de bateria y alimentacion externa
- **AGREGADO,** conector barrel jack a la alimentacion de la fuente regulada
- **AGREGADO,** controlador de bateria y carga TP4056

### **4-may-19**
- **AGREGADO,** Teclado con proteccion de rebote de los SW
- **AGREGADO,** Controlador de Lcd I2C PCF8574
- **AGREGADO,** Se cambio Proteus por KiCAD (Open Source)
- **AGREGADO,** rediseño de hardware para entrar en carcasa

***

## **[v0.8 - Beta - Firm v1.8 - v2.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.8/?at=master)**

- **AGREGADO,** los bordes para la union del acrílico
- **ELIMINADO,** base de teclado, queda sobre el acrílico
- **AGREGADO,** jack de DC para alimentacion
- **AGREGADO,** tornillo entre los conectores para agregar soporte
- **CORREGIDO,** bornera de voltaje lateral metera mas
- **CORREGIDO,** posición de SW esta al revés, cambiar leyenda en pcb

***

## **[v0.7 - Beta - Firm v1.8 - v2.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.7/?at=master)**

- **AGREGADO,** SW para Key Light
- **AGREGADO,** SW para Ext Light
- **AGREGADO,** Espaciadores para fijación
- **AGREGADO,** Leyendas de conexión
- **AGREGADO,** Fuente externa, el arduino se calentaba
- **CORREGIR,** marcar en board edge los agujeros de los XLR
- **CORREGIR,** los XLR quedan chuecos
- **CORREGIR,** Encoder, separar los agujeros están muy juntos
- **CORREGIR,** separar bornera de key light de lcd, esta muy junta
- **CORREGIR,** pads de puentes muy chicos
- **CORREGIR,** pads de botón de reset muy chicos
- **CORREGIR,** subir logo de Open Hardware
- **CORREGIR,** texto de about, hacerlo mas grueso
- **CORREGIR,** bornera de voltaje lateral metera mas
- **CORREGIR,** acostar C7
- **CORREGIR,** tornillos de LCD no coinciden
- **CORREGIR,** jumper de reset, poner pullup, el pin no tiene interno
- **CORREGIR,** posición de SW esta al revés, cambiar leyenda en pcb

***
 
## ** [v0.6 - Beta - Firm v1.7](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.6/?at=master)**

- **AGREGADO,** regulador para fuente externa, el arduino se calienta
- **AGREGADO,** bornera para salida de fuente regulada de 5V para uso general
- **AGREGADO,** led para salida de fuente regulada
- **AGREGADO,** botón de reset
- **AGREGADO,** opto acoplado a salida de dmx
- **AGREGADO,** convertidor de DC/DC para aislamiento de DMX
- **AGREGADO,** SW de palanca para invertir polaridad de salida de DMX
- **AGREGADO,** Al encoder capacitores para rebote
- **AGREGADO,** jumper default eeprom en pin 9
- **AGREGADO,** driver para luz led como lampara
- **AGREGADO,** capacitores al teclado para rebote
- **AGREGADO,** logo de Open Hardware
- **CORREGIDO,** se eliminan los cursores, los remplaza el encoder
- **CORREGIDO,** se saco el encoder de la placa original y se solda directo a la placa
- **CORREGIDO,** se elimina el data I/O
- **CORREGIGO,** se quito la entrada al Arduino desde el MAX485, causa problemas al cargar el programa
- **CORREGIDO,** Las bases de los CI se cambiaron a maquinadas
- **AGREGADO,** Optoacoplamiento basado en:
	- http://www.mathertel.de/Arduino/DMXShield.aspx
- **CHECAR,** en firmware que el encoder DT y CLK que sean pull-up

***

## ** [v0.5 - Beta - Firm v1.6 to 1.7](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.5/?at=master)**

- **CORREGIDO,** potenciómetro por encoder
- **CORREGIDO,** etapa de potencia para contraste
- **AGREGADO,** Luz para el teclado y cursores
- **AGREGADO,** Etapa de potencia para luz de teclado
- **AGREGADO,** Contraste por PWM basado en:
	- http://www.pcbheaven.com/circuitpages/PWM_LCD_Contrast_Control/

***

## **[v0.4 - Beta - Firm v0.9 to v1.3](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.4/Proteus/?at=master)**

- **CORREGIDO,** hacer más grandes los pads del SW
- **CORREGIDO,** quitar parte de abajo del soporte del teclado estorba con el flex del teclado
- **CORREGIDO,** recorrida la bornera de la conexión dmx hacia el arduino, queda más centrada
- **CORREGIDO,** acercar la base del mx485 al borde de la placa para que el teclado no estorbe
- **CORREGIDO,** agregar un diodo rectificador a la entrada de la batería
- **CORREGIDO,** se hizo más chico el puente que estaba dentro del área del arduino
- **CORREGIDO,** los datos de la salida están al revés
- **AGREGADO,** interconectados los pines del arduino de vcc y gnd faltantes
- **AGREGADO,** la leyenda open hardware a bottom

***

## ** [v0.3 - Beta - Firm v0.9 to v1.3](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.3/?at=master)**

- **AGREGADO,** backlight a pin 13 para efecto en el lcd mientras se actualiza
- **AGREGADO,** control de contraste desde software
- **AGREGADO,** control de back light desde software
- **CORREGIR,** agregar capacitores al backlight y al contraste, parpadea con baja frecuencia
- **CORREGIR,** acercar el potenciómetro a los pads y marcar el pad del centro
- **CORREGIR,** acercar la base del mx485 al borde de la placa para que el teclado no estorbe
- **CORREGIR,** la placa del teclado tiene mal ubicado el header de abajo donde pasa el flex del teclado
- **CORREGIR,** agregar un diodo rectificador a la entrada de la batería
- **CORREGIR,** agregar serigrafía a top en el pcb
- **CORREGIR,** hacer más grandes los pads del SW
- **CORREGIR,** quitar parte de abajo del soporte del teclado estorba con el flex del teclado
- **CORREGIR,** los datos de la salida están al revés

***

## ** [v0.2 - Beta - Firm v0.0 to v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.2/Proteus/?at=master)**

- **AGREGADO,** control de contraste desde software
- **AGREGADO,** control de back light desde software

***

## ** [v0.1 - Beta - Firm v0.0 to v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.1/?at=master)**

- **CORREGIDO,** no se necesita el led on, la pantalla ya tiene un led (el led ON se retira, los leds de la salida del dmx indican que está activo)
- **CORREGIDO,** no hay botón de reset (no se requiere el botón de reset, hay un switch de on off)
- **CORREGIDO,** quitar botón SW de encendido (se cambió por un switch deslizable)
- **CORREGIDO,** base de teclado, darle más soporte (no requiere más soporte)
- **CORREGIDO,** eliminar la fuente de alimentacion regulada (se eliminó la fuente regulada)
- **CORREGIDO,** quitar de los conectores USB y power del arduino, las conexiones de arriba, no permiten que cierre bien el arduino (se reubicaron los componentes)
- **CORREGIDO,** pasar los conectores a la parte de abajo, pesa mucho la placa (los conectores se pasaron a la parte de abajo)
- **CORREGIDO,** agregar capacitores cerámicos a los botones para el rebote (agregados, cerámicos 104, mejoro la estabilidad en la lectura)
- **CORREGIDO,** se cambió el conector XLR de 3 pin por conector amphenol de 3 y 5 pin, son más resistentes
- **CORREGIDO,** los leds de la salida del DMX se cambiaron a la parte de abajo y viendo hacia abajo, molestaba a la vista
- **CORREGIDO,** el pcb se redujo
- **AGREGADO,** etapa de potencia para controlar el back light del LCD
- **AGREGADO,** conexión para baterías
- **AGREGADO,** header para completar la conexión del arduino, estaba incompleta

***

## ** [v0.0 - Beta - Firm v0.0 to v0.8](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/03ac9d7d01c0ac0785e2351d8d0e76eea3e9af66/Hardware/v0.0/?at=master)**

- **CORREGIR,** no se necesita el led on, la pantalla ya tiene un led
- **CORREGIR,** no hay botón de reset
- **CORREGIR,** quitar botón SW de encendido
- **CORREGIR,** agregar canon XLR de 5 pin
- **CORREGIR,** base de teclado, darle más soporte
- **CORREGIR,** eliminar la fuente de alimentacion regulada, arduino ya tiene una fuente regulada
- **CORREGIR,** quitar de los conectores USB y power del arduino, las conexiones de arriba, no permiten que cierre bien el arduino
- **CORREGIR,** pasar los conectores a la parte de abajo, pesa mucho la placa
- **CORREGIR,** agregar capacitores cerámicos a los botones para el rebote

***