# **Repositorio**

## **[Firmware](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Firmware/?at=master)** 
**v2.2**

- Las versiones del Firmware están numeradas de manera consecutiva 
- La carpeta de "Documentación" contiene la información de las librerías utilizadas 
- La carpeta de "Documentación/Hardware" contiene la información del hardware específico 
- El archivo "Firmware - Notas Sobre la Version.md" contiene el histórico de cambios en las diferentes versiones del Firmware 
***

## **[Hardware](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Hardware/?at=master)** 
**v0.8**

- Las versiones del Hardware están numeradas de manera consecutiva 
- La carpeta de "Documentación/Componentes" contiene la información de los componentes específicos 
- El archivo "Hardware - Notas Sobre la Version.md" contiene el histórico de cambios en las diferentes versiones del Hardware 
***

## **[Simulador](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Simulador/?at=master)** 
**v0.2**

- Las versiones del Simulador están numeradas de manera consecutiva 
***

## **[Licencias](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Licencias/?at=master)** 
- Contiene la información sobre el licenciamiento del proyecto 
***

## **[Social](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Social/?at=master)** 
- Contiene la información de la actividad en Webs, Blogs y Redes Sociales 
***

## **[Software](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/4defa7c9bac5b5197830ca771d9cdd4b03aa369b/Software/?at=master)** 
- Contiene el software utilizado para el proyecto  
***