# ** Simulador **

***

## ** Notas sobre la versi�n **

- Simulacion en Proteus v7.7 SP2
- Las librerias necesarias estan en la carpeta Proteus Library
- Encoder: http://www.robodosis.net/2013/01/encoder-rotativo-mecanico-con-pic16f877a_2.html
- Arduino: https://geekelectronica.com/simular-arduino-con-proteus/

***

## ** [v0.2](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/ae80f9f0325731543896398e0f42f74f3f0f9755/Simulador/v0.2/?at=master) **

- Hardware v0.7 to v0.8
- Firmware v1.6 to v2.1

***

## ** [v0.1](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/ae80f9f0325731543896398e0f42f74f3f0f9755/Simulador/v0.1/?at=master) **

- Hardware v0.3 to v0.4
- Firmware v0.9 to v1.5

***

## ** [v0.0](https://bitbucket.org/daniel3514/0066-arduino-dmx-512-tester-controller/src/ae80f9f0325731543896398e0f42f74f3f0f9755/Simulador/v0.0/?at=master) **

- Hardware v0.0 to v0.2
- Firmware v0.0 to v0.8

***